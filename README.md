# Documentation: ILDA Digital Network (IDN) - Network Programming Playground (NPP)

In the IDN NP playground you can play around by sending UDP datagrams with very simply structured laser image data from a program 
written in any programming language of your own choice. The NPP tools are able to visualize the data on your computer monitor. 
Internally, the tools are using the ILDA Digital Network (IDN) stream specification. Thus, by just changing the configuration slightly, 
it is possible to send the laser image data also to real laser projectors!

There are several possibilities on how to get started:

*  read the programming documentation provided as PDF document (in German language) - [IDN-NPP_Programmierdokumentation_DE.pdf](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/Documentation/blob/master/IDN-NPP_Programmierdokumentation_DE.pdf)
*  have a look at the quick reference for network programming (also PDF, in English) - [IDN-NPP_Quick-Reference.pdf](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/Documentation/blob/master/IDN-NPP_Quick-Reference.pdf)
*  follow the remaining part of this README document
*  have a look at [gitlab Spoiler](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/Documentation/tree/master/Spoiler) where you can find several examples with photos/videos and source code in C, Java and Python
*  or a combination of all of these

## Most essential components to start playing in IDN-NPP

### a) Write your own program!

Write your own programm sending UDP datagrams with laser image data / sample point sequence. The documentation of data structures of the UDP payload
are given in the programming documentation (DE) or in the quick reference (EN).

The programming language is your choice!

The operating system / computer hardware is your choice (... as long as you can send UDP datagrams to a specific destination IP address and destination port).

### b) Start the IDN-Toolbox to visualize the laser data

On Windows: download and unpack the ZIP from [gitlab IDN-Toolbox](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/idn-tools/idn-toolbox)
and execute idn-toolbox.exe.

On Linux: (will follow later!)

### c) start LPM (the Laser Projection Module, aka LaProMo) to receive UDP datagrams (after done with step a)

LaProMo will convert the UDP datagrams into valid packets according to the ILDA Digital Network (IDN) stream specification.
The given laser image data (your sample point sequence) maybe extended by additional point samples using a very simple linear interpolation
to avoid damage to X-Y laser scanners if the distance between consecutive sample points is too large.

On Windows: download and unpack the LaProMo_LPM.zip from [gitlab LaProMo-LPM](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/idn-tools/lapromo-lpm)
and execute lapromo.IDN.exe. It will start in a separate command prompt window with some debug output, an additional IDN information window will pop up as well).

On Linux: (will follow later!)

### Hint for steps b) and c) (also d) and e): it may happen that you need to confirm Windows firewall settings to allow the programs to access the local network, and to allow Windows to execute unauthorized software.


### d) [OPTIONAL] Make a test before you start programming your own stuff! 

Use the existing Laser-Pong game to check if your setup with LPM and IDN-Toolbox is working!
Download one of the JAR files given at [gitlab Laser-Pong](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/idn-tools/laser-pong).

It will require an installation of a Java Runtime Environment (JRE) and is running on any operating system. It needs to execute on the same
computer where LaProMo is running.

### e) [OPTIONAL] Use the LaProMo Configuration GUI

You can use the LaProMo Configuration GUI to make changes to the basic configuration of the laser image data, like geometric correction
of laser display size, offset, flip x and y orientation, and others. This is usually only relevant when using real laser projectors.
You can also change the parameters for linear interpolation (be careful with real laser!).

On Windows: download and unpack the _Optional_LPM-Config-GUI.zip from [gitlab LaProMo-LPM](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/idn-tools/lapromo-lpm)
and execute LaProMo-Config.exe. 

A GUI will show up, use Settings Connect and select "127.0.0.1" from the list, and "import the settings"; on further things play around on your own.

On Linux: (will follow later!)


## ______________________________
##    IDN EXPERT INFORMATION 

If you have real laser projector(s) with IDN consumer components available, you can modify the settings of LaProMo (Laser Projection
Module) to send the IDN stream to your real laser projector!

You can change some settings in the file idn.conf (with any text editor) to either manually set an IP address of your IDN consumer
component or you can change the settings to use IDN service discovery. In case of questions on this, please consult with
the Uni Bonn / Laser & Light Lab team!
