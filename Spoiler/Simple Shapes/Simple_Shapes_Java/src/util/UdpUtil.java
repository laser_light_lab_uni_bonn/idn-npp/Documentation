package util;

import java.util.List;

import common.LaserPoint;

public class UdpUtil {

	public static byte[] createUdpLaserFrame(List<LaserPoint> points) {
		Integer length = 2 + points.size() * 8;
		byte[] data = new byte[length];

		data[0] = (byte) points.size();
		data[1] = (byte) (points.size() >>> 8);

		// Folge von je 8 Byte pro Bildpunkt
		int offset = 2;
		for (LaserPoint point : points) {
			byte[] pointData = point.getData();
			System.arraycopy(pointData, 0, data, offset, 8);
			offset = offset + 8;
		}

		return data;
	}

}
