package launch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import common.LaserPoint;
import net.UdpSender;

public class App {

	UdpSender sender;
	
	public static void main(String[] args) {
		App app = new App();
		if(args.length < 2)
			throw new IllegalArgumentException("IP-Adress oder Port nicht angegeben!");
		app.getInputData(args[0], args[1]);
	}

	private void getInputData(String adress, String portNr) {
		int port = 6677;
		try {
			port = Integer.parseInt(portNr);
		} catch (NumberFormatException  e) {
			System.out.println("Could not parse port number. Using default 6677.");
			port = 6677;
		}
		
		sender = new UdpSender(adress, port);
		if(sender == null)
			throw new NullPointerException("Could not create Udp sender.");
		
		System.out.println("Enter a figure you want to create: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.next();

		switch (input) {
		case "square":
			List<LaserPoint> points = createLaserSquareWhite(400, 400);
			sendLaserData(points, sender);
			break;
		case "color_square":
			List<LaserPoint> points2 = createLaserSquareColored(800, 250, "blue", "red", "blue", "green");
			sendLaserData(points2, sender);
			break;
		case "circle":
			List<LaserPoint> points3 = createLaserCircle(500, 50);
			sendLaserData(points3, sender);
			break;
		case "color_circle":
			sendColorChangingCircle();
			break;
		default:
			scanner.close();
			throw new IllegalArgumentException(String.format("%s is not a valid Argument.", input));
		}

		scanner.close();
	}

	private void sendColorChangingCircle() {
		
		int step = 0;
		long before = System.currentTimeMillis();
		while (true) {
			try {
				if (step == 360) {
					step = 0;
				}
				List<LaserPoint> points = createcolorChangingLaserCircle(1000, 50, step);
				sender.sendLaserData(points);
				long after = System.currentTimeMillis();

				//Warte bis Frame sich �ndert...
				if ((after - before) / 10 > 6) {
					before = System.currentTimeMillis();
					step = step + 1;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void sendLaserData(List<LaserPoint> points, UdpSender sender) {
		while (true) {
			try {
				sender.sendLaserData(points);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Creates white square LaserPoints
	 * 
	 * @param w width of square
	 * @param h height of square
	 * @return List of LaserPoints
	 */
	public static List<LaserPoint> createLaserSquareWhite(int w, int h) {
		return createLaserSquareColored(w, h, "white", "white", "white", "white");
	}

	public static List<LaserPoint> createLaserSquareColored(int w, int h, String colorW1, String colorH1,
			String colorW2, String colorH2) {
		List<LaserPoint> points = new ArrayList<LaserPoint>();

		LaserPoint point1 = new LaserPoint();
		point1.setX(0);
		point1.setY(0);
		colorStringToLaserColor(point1, colorH1);
		point1.setIntensity(255);
		points.add(0, point1);

		LaserPoint point2 = new LaserPoint();
		point2.setX(w - 1);
		point2.setY(0);
		colorStringToLaserColor(point2, colorW1);
		point2.setIntensity(255);
		points.add(1, point2);

		LaserPoint point3 = new LaserPoint();
		point3.setX(w);
		point3.setY(h);
		colorStringToLaserColor(point3, colorH2);
		point3.setIntensity(255);
		points.add(2, point3);

		LaserPoint point4 = new LaserPoint();
		point4.setX(1);
		point4.setY(h - 1);
		colorStringToLaserColor(point4, colorW2);
		point4.setIntensity(255);
		points.add(3, point4);

		LaserPoint point = new LaserPoint();
		point.setX(0);
		point.setY(0);
		colorStringToLaserColor(point, colorH1);
		point.setIntensity(255);
		points.add(4, point);

		return points;
	}

	public static List<LaserPoint> createLaserCircle(int radius, int accuracy) {
		List<LaserPoint> points = new ArrayList<LaserPoint>();

		double angle = 2 * Math.PI / accuracy;

		for (int i = 0; i < accuracy; i++) {
			float x = (float) (Math.cos(i * angle) * radius) + 1024;
			float y = (float) (Math.sin(i * angle) * radius) + 1024;

			LaserPoint point = new LaserPoint();
			point.setX(Math.round(x));
			point.setY(Math.round(y));
			colorStringToLaserColor(point, "white");

			points.add(point);
		}

		LaserPoint lastP = points.get(0);
		points.add(lastP);

		return points;
	}

	private static LaserPoint colorStringToLaserColor(LaserPoint point, String color) {

		switch (color) {
		case "green":
			point.setGreen(255);
			point.setBlue(0);
			point.setRed(0);
			break;

		case "red":
			point.setGreen(0);
			point.setBlue(0);
			point.setRed(255);
			break;

		case "blue":
			point.setGreen(0);
			point.setBlue(255);
			point.setRed(0);
			break;

		case "white":
			point.setGreen(255);
			point.setBlue(255);
			point.setRed(255);
			break;

		default:
			System.out.println("Color doesn't exists. Using default: white.");
			point.setGreen(255);
			point.setBlue(255);
			point.setRed(255);
		}

		return point;
	}

	private static List<LaserPoint> createcolorChangingLaserCircle(int radius, int accuracy, int step) {
		List<LaserPoint> points = new ArrayList<LaserPoint>();

		double angle = 2 * Math.PI / accuracy;

		for (int i = 0; i < accuracy; i++) {
			float x = (float) (Math.cos((i + step) * angle) * radius) + 1024;
			float y = (float) (Math.sin((i + step) * angle) * radius) + 1024;

			float newAng = (float) Math.toDegrees(i * angle);

			LaserPoint point = new LaserPoint();
			point.setX(Math.round(x));
			point.setY(Math.round(y));

			int blue = 0;
			int red = 0;
			int green = 0;

			if (newAng < 60) {
				red = 255;
				green = Math.round((float) (newAng * 4.25 - 0.01));
				blue = 0;
			} else if (newAng < 120) {
				red = Math.round((float) ((120 - newAng) * 4.25 - 0.01));
				green = 255;
				blue = 0;
			} else if (newAng < 180) {
				red = 0;
				green = 255;
				blue = Math.round((float) ((newAng - 120) * 4.25 - 0.01));
			} else if (newAng < 240) {
				red = 0;
				green = Math.round((float) ((240 - newAng) * 4.25 - 0.01));
				blue = 255;
			} else if (newAng < 300) {
				red = Math.round((float) ((newAng - 240) * 4.25 - 0.01));
				green = 0;
				blue = 255;
			} else {
				red = 255;
				green = 0;
				blue = Math.round((float) ((360 - newAng) * 4.25 - 0.01));
			}
			point.setBlue(blue);
			point.setGreen(green);
			point.setRed(red);

			points.add(point);
		}

		LaserPoint lastP = points.get(0);
		points.add(lastP);

		return points;
	}

}
