package net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import common.LaserPoint;

public class UdpSender {

	private DatagramSocket socket;
	private InetAddress address;
	private int port;

	public UdpSender(String address, int port) {
		this.port = port;
		try {
			this.address = Inet4Address.getByName(address);

		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		System.out.println(String.format("Sending to %s on port %d", this.address, this.port));

	}

	public void sendLaserData(List<LaserPoint> points) throws IOException {
		socket = new DatagramSocket();
		byte[] data = createUdpLaserFrame(points);
		DatagramPacket pack = new DatagramPacket(data, data.length, address, port);
		socket.send(pack);
		socket.close();
	}

	public static byte[] createUdpLaserFrame(List<LaserPoint> points) {
		Integer length = 2 + points.size() * 8;
		byte[] data = new byte[length];

		data[0] = (byte) points.size();
		data[1] = (byte) (points.size() >>> 8);

		// Folge von je 8 Byte pro Bildpunkt
		int offset = 2;
		for (LaserPoint point : points) {
			byte[] pointData = point.getData();
			System.arraycopy(pointData, 0, data, offset, 8);
			offset = offset + 8;
		}

		return data;
	}

}
