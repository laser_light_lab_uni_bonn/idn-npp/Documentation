package common;

public class LaserPoint {

	private int x;
	private int y;
	private int red;
	private int green;
	private int blue;
	private int intensity;

	private byte[] data;

	public LaserPoint() {
		this.x = 0;
		this.y = 0;
		this.red = 0;
		this.green = 0;
		this.blue = 0;
		this.intensity = 0;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		if (x > 4095) {
			System.out.println("X to big. Using default 4095.");
			this.x = 4095;
		} else if (x < 0) {
			System.out.println("X to small. Using default 0.");
			this.x = 0;
		} else {
			this.x = x;
		}
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		if (y > 4095) {
			System.out.println("Y to big. Using default 4095.");
			this.y = 4095;
		} else if (y < 0) {
			System.out.println("Y to small. Using default 0.");
			this.y = 0;
		} else {
			this.y = y;
		}
	}

	public int getRed() {
		return red;
	}

	public void setRed(int red) {
		if (red > 255) {
			System.out.println("Red to big. Using default 255.");
			this.red = 255;
		} else if (red < 0) {
			System.out.println("Red to small. Using default 0.");
			this.red = 0;
		} else {
			this.red = red;
		}
	}

	public int getGreen() {
		return green;
	}

	public void setGreen(int green) {
		if (green > 255) {
			System.out.println("Green to big. Using default 255.");
			this.green = 255;
		} else if (green < 0) {
			System.out.println("Green to small. Using default 0.");
			this.green = 0;
		} else {
			this.green = green;
		}
	}

	public int getBlue() {
		return blue;
	}

	public void setBlue(int blue) {
		if (blue > 255) {
			System.out.println("Blue to big. Using default 255.");
			this.blue = 255;
		} else if (blue < 0) {
			System.out.println("Blue to small. Using default 0.");
			this.blue = 0;
		} else {
			this.blue = blue;
		}
	}

	public int getIntensity() {
		return intensity;
	}

	public void setIntensity(int intensity) {
		if (intensity > 255) {
			System.out.println("Intensity to big. Using default 255.");
			this.intensity = 255;
		} else if (intensity < 0) {
			System.out.println("Intensity to small. Using default 0.");
			this.intensity = 0;
		} else {
			this.intensity = intensity;
		}
	}

	public byte[] getData() {
		this.data = new byte[8];
		this.data[1] = (byte) (this.x >>> 8);
		this.data[0] = (byte) this.x;
		this.data[3] = (byte) (this.y >>> 8);
		this.data[2] = (byte) this.y;
		this.data[4] = (byte) this.red;
		this.data[5] = (byte) this.green;
		this.data[6] = (byte) this.blue;
		this.data[7] = (byte) this.intensity;
		return this.data;
	}

	@Override
	public String toString() {
		return "LaserPoint [x=" + x + ", y=" + y + ", red=" + red + ", green=" + green + ", blue=" + blue
				+ ", intensity=" + intensity + "]";
	}
	
	

}
