#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket, sys, time, numpy as np, cmath as ma, signal
"""
Created on Mon Oct 28 14:53:15 2019

@author: Georg Martin Kuhlemann
"""
"""
This Funktion selects the low bytes of a 16-Bit number
"""
def fst(x):
    return np.uint8(int(x) & 0x00FF)

"""
This Function selects the high bytes of a 16-Bit number
"""
def snd(x):
    return np.uint8((int(x) & 0xFF00) >> 8)

"""
This Funktion adds a 16-Bit number in little endian byteorder into a numpy array
"""
def addLitEnd(Arr, Coord):
    Arr += [fst(Coord)]
    Arr += [snd(Coord)]
"""
This Function adds 8-Bit rgb data to a numpy array
"""
def addRGBI(Points,r,g,b,i):
    Points += [np.uint8(r)]
    Points += [np.uint8(g)]
    Points += [np.uint8(b)]
    Points += [np.uint8(i)]

def stop(sig,frame):
    Stop = []*2
    Stop += [fst(0)]
    Stop += [snd(194)]
    sock.sendto(bytes(Stop), (IP, Port))
    sys.exit()

"""
This Function takes in a IPv4 address and Portnumber
and sends a blue circle to LaProMo using UDP
"""
IP = ""
Port = 0
sock = socket.socket(socket.AF_INET,                                           #Sockettype: IPv4
                     socket.SOCK_DGRAM                                         #Protocol:   UDP
                     )
def main(argv):
    #Extract IP and Portnumber
    if(len(argv) != 4):
        print("Useage: " + argv[0] + " <IP> <Port> <Angle>\n")
        sys.exit()
    global IP
    global Port
    IP   = argv[1]
    Port = int(argv[2])
    Angle  = int(argv[3])

    Rad    = 2000
    OffsetX = (2**12 - 1) // 2
    OffsetY = OffsetX

    signal.signal(signal.SIGINT, stop)
    #Fill Array with Points
    NumPnts = np.uint16(int(360/Angle)+1)
    Points = []*(2+NumPnts*8)
    addLitEnd(Points,NumPnts)                                                  #Number of Points in Little Endian
    angle_cmplx = ma.exp((Angle*ma.pi/180)*1j)                                 #Determine point being radius 1 from origin with angle "Angle" away from x axis
    phi = 0
    z   = 1
    while(phi < 360):                                                          #Turn z by phi until its over 360 degrees
        addLitEnd(Points,Rad*z.real + OffsetX)                                 #Add x coordinate with radius Rad and OffsetX
        addLitEnd(Points,Rad*z.imag + OffsetY)                                 #Add y coordinate with radius Rad and OffsetY
        addRGBI(Points,0,0,255,0)
        z *= angle_cmplx                                                       #Turn z by phi using complex multiplication
        phi += Angle

    addLitEnd(Points,Rad*1 + OffsetX)                                          #X Coordinate Connect Cricle
    addLitEnd(Points,Rad*0 + OffsetY)                                          #Y Coordinate
    addRGBI(Points,0,0,255,0)

    while True:
        sock.sendto(bytes(Points), (IP, Port))                                 #Send Points
        time.sleep(1/30)

#Only execute in interpreter
if __name__ == "__main__":
    main(sys.argv)
