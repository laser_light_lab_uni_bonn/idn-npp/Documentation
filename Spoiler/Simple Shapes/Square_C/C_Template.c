#include<unistd.h>
#include<signal.h>
#include<string.h>
#include<stdio.h>
#include<math.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<stdlib.h>
#include<netinet/in.h>
#include<arpa/inet.h>

int fd = 0;
static void
close_soc (int sig)
{
  //Schließen des Sockets
  int err = close (fd);
  if (err < 0)
    fprintf (stdout, "Failed to close Socket; Signal received %i\n", sig);
  puts ("\nexiting");
  exit (0);
}

int
main (int argc, char *argv[])
{
  if (argc != 3)
    {				//Fehlerbehandlung Argumentanzahl
      fprintf (stderr, "Useage: %s <IP> <Port>\n",argv[0]);
      exit (-1);
    }
  struct sockaddr_in dest;
  signal (SIGINT, close_soc);	//Registrieren von Signalhandler: KeyboardInterrupt
  fd = socket (AF_INET, SOCK_DGRAM, 0);	//Erstellen des Sockets
  if (fd < 0)
    {
      fprintf (stderr, "Failed to create own socket\n");
      exit (-1);
    }
  int err = 0;			//Errorcode
  int n = 5;			//Anzahl Bildpunkte
  unsigned char msg[2 + n * 8];	//Buffer
  unsigned short ptsX[n];
  unsigned short ptsY[n];

  dest.sin_family = AF_INET;	//Ziel eintragen
  dest.sin_port = htons (strtol (argv[2], NULL, 10));
  dest.sin_addr.s_addr = inet_addr (argv[1]);

  bzero (&msg, sizeof (msg));	//Buffer leeren
  bzero (&ptsX, sizeof (ptsX));
  bzero (&ptsY, sizeof (ptsY));
  ptsX[0] = (pow(2,12) - 1) * (3.0/7);		//Nachricht generieren
  ptsY[0] = (pow(2,12) - 1) * (3.0/7);

  ptsX[1] = (pow(2,12) - 1) * (5.0/7);
  ptsY[1] = (pow(2,12) - 1) * (3.0/7);

  ptsX[2] = (pow(2,12) - 1) * (5.0/7);
  ptsY[2] = (pow(2,12) - 1) * (5.0/7);

  ptsX[3] = (pow(2,12) - 1) * (3.0/7);
  ptsY[3] = (pow(2,12) - 1) * (5.0/7);

  ptsX[4] = (pow(2,12) - 1) * (3.0/7);
  ptsY[4] = (pow(2,12) - 1) * (3.0/7);

  msg[0] |= n & 0x00FF;		//Bildpunkte in Nachricht eintragen
  msg[1] |= (n & 0xFF00) >> 8;
  for (int i = 0; i < n; i++)
    {
      msg[2 + 8 * i + 0] = (char) (ptsX[i]  & 0x00FF);	//Byteorder beachten und Punkte eintragen
      msg[2 + 8 * i + 1] = (char) ((ptsX[i] & 0xFF00) >> 8);
      msg[2 + 8 * i + 2] = (char) (ptsY[i]  & 0x00FF);
      msg[2 + 8 * i + 3] = (char) ((ptsY[i] & 0xFF00) >> 8);
      msg[2 + 8 * i + 4] = 255;	//Farbe Rot
      msg[2 + 8 * i + 5] = 0;	//Farbe Grün
      msg[2 + 8 * i + 6] = 0;	//Farbe Blau
      msg[2 + 8 * i + 7] = 255;	//Intensität
    }
  while(1)
    {
      err = sendto (fd, msg, 2 + n * 8, 0, (struct sockaddr *) &dest, sizeof (struct sockaddr_in));
      //Nachricht an Ziel senden
      usleep(100000);
    }

  close_soc (err);		//Fehlerbehandlung
}
