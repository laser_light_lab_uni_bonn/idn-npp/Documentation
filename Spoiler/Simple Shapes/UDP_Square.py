#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket, sys, time, numpy as np, signal
"""
Created on Mon Oct 28 14:53:15 2019

@author: Georg Martin Kuhlemann
"""
"""
This Funktion selects the low bytes of a 16-Bit number
"""
def fst(x):
    return np.uint8(int(x) & 0x00FF)

"""
This Function selects the high bytes of a 16-Bit number
"""
def snd(x):
    return np.uint8((int(x) & 0xFF00) >> 8)

"""
This Funktion adds a 16-Bit number in little endian byteorder into a numpy array
"""
def addLitEnd(Arr, Coord):
    Arr += [fst(Coord)]
    Arr += [snd(Coord)]
"""
This Function adds 8-Bit rgb data to a numpy array
"""
def addRGBI(Points,r,g,b,i):
    Points += [np.uint8(r)]
    Points += [np.uint8(g)]
    Points += [np.uint8(b)]
    Points += [np.uint8(i)]

def stop(sig,frame):
    Stop = []*2
    Stop += [fst(0)]
    Stop += [snd(194)]
    sock.sendto(bytes(Stop), (IP, Port))
    sys.exit()
"""
This Function takes in a IPv4 address and Portnumber
and sends an encoded square to LaProMo using UDP
"""
IP = ""
Port = 0
sock = socket.socket(socket.AF_INET,                                            #Sockettype: IPv4
                     socket.SOCK_DGRAM                                          #Protocol:   UDP
                     )
def main(argv):
    #Extract IP and Portnumber
    global IP
    global Port
    if(len(argv) != 3):
        print("Useage: \n" + argv[0] + " <IP> <Port>\n")
        sys.exit()
    IP   = argv[1]
    Port = int(argv[2])

    signal.signal(signal.SIGINT, stop)
    #Fill Array with Points
    NumPnts = np.uint16(5)
    Points = []*(2+NumPnts*8)
    addLitEnd(Points,NumPnts)            #Number of Points in Little Endian

    #The Points  must be in grey Code and end were they started
    addLitEnd(Points,(2**12 - 1) * (3/7))#X Coordinate 1
    addLitEnd(Points,(2**12 - 1) * (3/7))#Y Coordinate
    addRGBI(Points,255,0,0,0)

    addLitEnd(Points,(2**12 - 1) * (5/7))#X Coordinate 2
    addLitEnd(Points,(2**12 - 1) * (3/7))#Y Coordinate
    addRGBI(Points,255,0,0,0)

    addLitEnd(Points,(2**12 - 1) * (5/7))#X Coordinate 3
    addLitEnd(Points,(2**12 - 1) * (5/7))#Y Coordinate
    addRGBI(Points,255,0,0,0)

    addLitEnd(Points,(2**12 - 1) * (3/7))#X Coordinate 4
    addLitEnd(Points,(2**12 - 1) * (5/7))#Y Coordinate
    addRGBI(Points,255,0,0,0)

    addLitEnd(Points,(2**12 - 1) * (3/7))#X Coordinate 5
    addLitEnd(Points,(2**12 - 1) * (3/7))#Y Coordinate
    addRGBI(Points,255,0,0,0)
    
    while True:
        sock.sendto(bytes(Points), (IP, Port))#Send Points
        time.sleep(1/30)

#Only execute in interpreter
if __name__ == "__main__":
    main(sys.argv)
