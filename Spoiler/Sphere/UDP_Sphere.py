#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import socket, sys, time, numpy as np, cmath as ma, signal
"""
Created on Mon Oct 28 14:53:15 2019

@author: Georg Martin Kuhlemann
"""
"""
This Funktion selects the low bytes of a 16-Bit number
"""
def fst(x):
    return np.uint8(int(x) & 0x00FF)

"""
This Function selects the high bytes of a 16-Bit number
"""
def snd(x):
    return np.uint8((int(x) & 0xFF00) >> 8)

"""
This Funktion adds a 16-Bit number in little endian byteorder into a numpy array
"""
def addLitEnd(Arr, Coord):
    Arr += [fst(Coord)]
    Arr += [snd(Coord)]
"""
This Function adds 8-Bit rgb data to a numpy array
"""
def addRGBI(Points,r,g,b,i):
    Points += [np.uint8(r)]
    Points += [np.uint8(g)]
    Points += [np.uint8(b)]
    Points += [np.uint8(i)]
"""
Creates a numpy array of sphere with a given radius and Angles
"""
def createSphere(AngleCir,AngleSph,Rad):
    NumPnts = np.uint16(int(360/AngleCir)+1)                                   #Number of points in a circle
    Points  = []*(int(NumPnts*3*(180/AngleSph)))                               #1D array of sphere

    angle_cmplx = ma.exp((AngleCir*ma.pi/180)*1j)                              #Determine point being radius 1 from origin with angle "Angle" away from x axis
    phi = 0
    z   = 1j
    while(phi <= 360):                                                         #Create sphere in xz-plane
        Points += [Rad*z.real]                                                 #X
        Points += [0]                                                          #Y
        Points += [Rad*z.imag]                                                 #Z
        z *= angle_cmplx
        phi += AngleCir
    phi = 0
    pnt = 0
    angle_cmplx = ma.exp((AngleSph*ma.pi/180)*1j)
    while(phi < 180):                                                          #Turn circle around z axis
        xy      = Points[3*pnt] + Points[3*pnt+1]*1j
        xy     *= angle_cmplx
        Points += [xy.real]
        Points += [xy.imag]
        Points += [Points[3*pnt+2]]
        if(pnt % NumPnts == NumPnts-1):                                        #if a circle turn is done, increase angle of xy rotation
            phi += AngleSph
        pnt    += 1
    return np.reshape(Points, (int(NumPnts*(180/AngleSph + 1)), 3)).T
"""
Turns the Camera a given Angle
"""
def turnCamera(AngleXY,AngleYZ,Camera,CameraOff):
    #Turn by AngleYZ in YZ-plane
    angle_cmplx = ma.exp((AngleYZ*ma.pi/180)*1j)
    z = Camera[1,0] + Camera[2,0]*1j
    z *= angle_cmplx
    Camera[1,0] = z.real
    Camera[2,0] = z.imag

    z  = Camera[1,1] + Camera[2,1]*1j
    z *= angle_cmplx
    Camera[1,1] = z.real
    Camera[2,1] = z.imag

    z  = CameraOff[1,0] + CameraOff[2,0]*1j
    z *= angle_cmplx
    CameraOff[1,0] = z.real
    CameraOff[2,0] = z.imag

    #Turn by AngleXY in XY-plane
    angle_cmplx = ma.exp((AngleXY*ma.pi/180)*1j)
    z = Camera[0,0] + Camera[1,0]*1j
    z *= angle_cmplx
    Camera[0,0] = z.real
    Camera[1,0] = z.imag

    z  = Camera[0,1] + Camera[1,1]*1j
    z *= angle_cmplx
    Camera[0,1] = z.real
    Camera[1,1] = z.imag

    z  = CameraOff[0,0] + CameraOff[1,0]*1j
    z *= angle_cmplx
    CameraOff[0,0] = z.real
    CameraOff[1,0] = z.imag
    return (Camera, CameraOff)
"""
Trows out Points, that are not visible from the cameras perspective. This Function only applies to a sphere.
"""
def visible(Camera, Points):
    DVec = np.cross(Camera[:,[0]].T, Camera[:,[1]].T)                          #Crossproduct of camara coordiantes
    return ((-1*DVec) @ Points)                                                #Determine which points are visible or not
"""
Projects Points orthogonal with respect to the camera
"""
def project(Camera,Rad,Points3D):
    #Points3D = Points3D - np.repeat(CameraOff,Points3D.shape[1],axis=1)       #Applys the cameras offset to points
    return np.linalg.lstsq(Camera,Points3D,rcond=None)[0]
"""
Applies an offset to the projected Points
"""
def offset(x, y, Points2D):
    ofs = np.array([[x],[y]])
    return Points2D + np.repeat(ofs,Points2D.shape[1],axis=1)

def stop(sig,frame):
    Stop = []*2
    Stop += [fst(0)]
    Stop += [snd(194)]
    sock.sendto(bytes(Stop), (IP, Port))
    sys.exit()

IP = ""
Port = 0
sock = socket.socket(socket.AF_INET,                                       #Sockettype: IPv4
                     socket.SOCK_DGRAM                                     #Protocol:   UDP
                     )
"""
This Function takes in a IPv4 address and Portnumber
and sends a blue sphere to LaProMo using UDP
"""
def main(argv):
    #Extract IP and Portnumber
    if(len(argv) != 5):
        print("Useage: " + argv[0] +" <IP> <Port> <AngleCir> <AngelSph>\n")
        sys.exit()
    global IP
    global Port
    IP   = argv[1]
    Port = int(argv[2])
    Rad = 2000
    CirRad = int(argv[3])
    SphRad = int(argv[4])
    
    signal.signal(signal.SIGINT,stop)
    #Fill Array with Points
    Points3D = createSphere(CirRad,SphRad,Rad)
    Camera = np.zeros((3,2))
    Camera[2,1] = 1.0
    Camera[0,0] = 1.0
    CameraOff = np.zeros((3,1))
    CameraOff[1,0] = -4000.0
    (Camera, CameraOff) = turnCamera(20, 45, Camera, CameraOff)                #Turn Camara 20 Degrees XY and 45 Degrees XZ
    vis = visible(Camera, Points3D)                                            #Create decision Array; >=0 Visible,<0 invisible
    Points2D = project(Camera,Rad,Points3D)
    Points2D = offset(2000,2000,Points2D)                                      #Move Points in 2D Space
    if vis[0][0] >= 0:
        invis = 0
    else:
        invis = 1                                                              #Indicate change in visibility
    cnt = 1                                                                    #Count number of points
    i = 1
    while i < vis.shape[1]:
        if (vis[0][i] >= 0) & (invis == 0):                                    #Count how many points must be sent
            cnt += 1
        elif (vis[0][i] < 0) & (invis == 0):
            cnt += 2
            invis = 1
        elif (vis[0][i] >= 0) & (invis == 1):
            cnt += 2
            invis = 0
        i += 1
    Points = []*(2 + 8*cnt)
    addLitEnd(Points,cnt)
    i = 1
    addLitEnd(Points,Points2D[0][0])                                           #Add first point
    addLitEnd(Points,Points2D[1][0])
    if vis[0][0] >= 0:
        addRGBI(Points,0,0,255,0)
        invis = 0
    else:
        addRGBI(Points,0,0,0,0)
        invis = 1
    prev = Points2D[:,[0]]
    while i < vis.shape[1]:
        if (vis[0][i] >= 0) & (invis == 0):                                    #Draw Points and connect them depending on their visibiliy
            addLitEnd(Points,Points2D[0][i])
            addLitEnd(Points,Points2D[1][i])
            addRGBI(Points,0,0,255,0)
            prev = Points2D[:,[i]]
        elif (vis[0][i] < 0) & (invis == 0):                                   #Becoming invisible
            addLitEnd(Points,prev[0][0])
            addLitEnd(Points,prev[1][0])
            addRGBI(Points,0,0,0,0)
            addLitEnd(Points,Points2D[0][i])
            addLitEnd(Points,Points2D[1][i])
            addRGBI(Points,0,0,0,0)
            prev = Points2D[:,[i]]
            invis = 1
        elif (vis[0][i] >= 0) & (invis == 1):                                  #Becoming visible
            addLitEnd(Points,Points2D[0][i])
            addLitEnd(Points,Points2D[1][i])
            addRGBI(Points,0,0,0,0)
            addLitEnd(Points,Points2D[0][i])
            addLitEnd(Points,Points2D[1][i])
            addRGBI(Points,0,0,255,0)
            invis = 0
            prev = Points2D[:,[i]]
        i += 1
        
    A = bytes(Points)
    while True:
        sock.sendto(A, (IP, Port))                                             #Send Points
        time.sleep(1/30)

#Only execute in interpreter
if __name__ == "__main__":
    main(sys.argv)
